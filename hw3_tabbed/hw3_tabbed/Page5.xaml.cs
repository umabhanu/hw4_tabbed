﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.TizenSpecific;
using Xamarin.Forms.Xaml;

namespace hw3_tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page5 : ContentPage
    {
        int clickTotal;
        int clickTotal1;
        public Page5()
        {
            InitializeComponent();
        }
     async   void OnImageButtonClicked(object sender, EventArgs e) 
        {
            clickTotal += 1;
            label.Text = $"{clickTotal} firstImageButton click{(clickTotal == 1 ? "" : "s")}";      //counts clicks for first image
            bool usersResponse = await DisplayAlert("hello", "do you want to know my name", "yes", "no"); //alert popup for first image

            if (usersResponse == true)
            {
                label.Text = $"my name is pigeon.";
            }

        }
       async void OnImageButtonClicked1(object sender, EventArgs e)
        {
            clickTotal1 += 1;
            label.Text = $"{clickTotal1} second ImageButton click{(clickTotal1 == 1 ? "" : "s")}";   //counts clicks for second image
            bool usersResponse = await DisplayAlert("hello", "do you want to know my name", "yes", "no"); //alert popup for second image

            if (usersResponse == true) 
            {
                label.Text = $"my name is sparrow.";
            }
        }


        async void BackButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync(); //pop taken back to its root page i.e., page 2
        }
    }
}