﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace hw3_tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page6 : ContentPage
    {
        public Page6()
        {
			Title = "Rotate Animation";

			image = new Image { Source = ImageSource.FromFile("bunny.png"), VerticalOptions = LayoutOptions.CenterAndExpand }; //image which on rotation is done
			startButton = new Button { Text = "Start Animation", VerticalOptions = LayoutOptions.End }; // rotation controller
			cancelButton = new Button { Text = "Cancel Animation", IsEnabled = false }; //rotation controller

			startButton.Clicked += OnStartAnimationButtonClicked;
			cancelButton.Clicked += OnCancelAnimationButtonClicked;

			Content = new StackLayout
			{
				Margin = new Thickness(0, 20, 0, 0),
				Children = {
					image,
					startButton,
					cancelButton
				}
			};
		}

		void SetIsEnabledButtonState(bool startButtonState, bool cancelButtonState)
		{
			startButton.IsEnabled = startButtonState;
			cancelButton.IsEnabled = cancelButtonState;
		}

		async void OnStartAnimationButtonClicked(object sender, EventArgs e)
		{
			SetIsEnabledButtonState(false, true);

			await image.RotateTo(360, 2000);
			image.Rotation = 0;

			SetIsEnabledButtonState(true, false);
		}

		void OnCancelAnimationButtonClicked(object sender, EventArgs e)
		{
			ViewExtensions.CancelAnimations(image);
			SetIsEnabledButtonState(true, false);
		}
	}
}