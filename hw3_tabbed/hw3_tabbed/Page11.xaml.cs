﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace hw3_tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page11 : ContentPage
    {
        public Page11()
        {
            InitializeComponent();
        }
		void SetIsEnabledButtonState(bool startButtonState, bool cancelButtonState)
		{
			startButton.IsEnabled = startButtonState;
			cancelButton.IsEnabled = cancelButtonState;
		}

		async void OnStartAnimationButtonClicked(object sender, EventArgs e) // fade animation code
		{
			SetIsEnabledButtonState(false, true);

			image.Opacity = 0;
			await image.FadeTo(1, 4000);

			SetIsEnabledButtonState(true, false);
		}

		void OnCancelAnimationButtonClicked(object sender, EventArgs e) // cancel image animation and gets back to normal position.
		{
			ViewExtensions.CancelAnimations(image);
			SetIsEnabledButtonState(true, false);
		}
	}
}
    
