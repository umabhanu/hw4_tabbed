﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace hw3_tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page3 : ContentPage
    {
        public Page3()
        {
            InitializeComponent();
        }
        async void OnRotateAnimationButtonClicked1(object sender, EventArgs e)
        {
            bool usersResponse = await DisplayAlert("hello", "do you want to see rotate animation cat ", "yes", "no"); //alert popup for first image

            if (usersResponse == true)
            {
                await Navigation.PushAsync(new Page8()); //navigated to page 8
            }
            else
            {
                await Navigation.PushAsync(new Page10()); //navigated to page 10
            }
        }
        async void OnFadeAnimationButtonClicked1(object sender, EventArgs e)
        {
            bool usersResponse = await DisplayAlert("hello", "do you want to see faded animation for cat", "yes", "no"); //alert popup for first image

            if (usersResponse == true)
            {
                await Navigation.PushAsync(new Page9()); //navigated to page 9

            }
            else
            {
                await Navigation.PushAsync(new Page11()); //navigated to page 11
            }
        }
    }
}