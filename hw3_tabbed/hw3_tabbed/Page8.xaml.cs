﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace hw3_tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page8 : ContentPage
    {
        public Page8()
        {
			Title = "Rotate Animation";

			image = new Image { Source = ImageSource.FromFile("cat1.png"), VerticalOptions = LayoutOptions.CenterAndExpand }; //image on which rotation is performed
			startButton = new Button { Text = "Start Animation", VerticalOptions = LayoutOptions.End };
			cancelButton = new Button { Text = "Cancel Animation", IsEnabled = false };

			startButton.Clicked += OnStartAnimationButtonClicked;
			cancelButton.Clicked += OnCancelAnimationButtonClicked;

			Content = new StackLayout
			{
				Margin = new Thickness(0, 20, 0, 0),
				Children = {
					image,
					startButton,
					cancelButton
				}
			};
		}

		void SetIsEnabledButtonState(bool startButtonState, bool cancelButtonState)
		{
			startButton.IsEnabled = startButtonState;
			cancelButton.IsEnabled = cancelButtonState;
		}

		async void OnStartAnimationButtonClicked(object sender, EventArgs e) // when statrt button is pressed this method will get executed
		{
			SetIsEnabledButtonState(false, true);

			await image.RotateTo(360, 2000);
			image.Rotation = 0;

			SetIsEnabledButtonState(true, false);
		}

		void OnCancelAnimationButtonClicked(object sender, EventArgs e) // when cancel button is pressed this method will get executed
		{
			ViewExtensions.CancelAnimations(image);
			SetIsEnabledButtonState(true, false);
		}
		async void BackButtonClicked(object sender, EventArgs e)
		{
			await Navigation.PopAsync(); //pop taken back to its root page i.e., page 2
		}
	}
}