﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace hw3_tabbed
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new  MainTab(); // set as root page which is tabbed page
            

        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
