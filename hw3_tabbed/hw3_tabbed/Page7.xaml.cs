﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace hw3_tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page7 : ContentPage
    {
        public Page7()
        {
			InitializeComponent();
		}

		void SetIsEnabledButtonState(bool startButtonState, bool cancelButtonState)
		{
			startButton.IsEnabled = startButtonState;
			cancelButton.IsEnabled = cancelButtonState;
		}

		async void OnStartAnimationButtonClicked(object sender, EventArgs e) //when start button is pressed on screen this action will perform
		{
			SetIsEnabledButtonState(false, true);

			image.Opacity = 0;
			await image.FadeTo(1, 4000);

			SetIsEnabledButtonState(true, false);
		}

		void OnCancelAnimationButtonClicked(object sender, EventArgs e) //when cancel button is pressed on screen this action will perform
		{
			ViewExtensions.CancelAnimations(image);
			SetIsEnabledButtonState(true, false);
		}
	}
}

